//Voorbeeld om promisses beter te gaan begrijpen. 
//In dit voorbeeld wordt er gebruik gemaakt van arrow functions en log_helder.js die het loggen van het resultaat verzorgd

/**
  * Helper 'belofte' functie voert een resolve of een reject uit met een delay van 1 - 2 seconden
  *
  * @att resolve()
  * @att reject()
  * @att label, string
  * @att betrouwbaarheid, double [0,1]
  * 
  * Voorbeeld gebruik: belofte(resolve,reject,'beloofdIsBeloofd',0.5);
  */
var belofte = ((resolve, reject, naam, betrouwbaarheid) => {
  var resultaat = (Math.random() >= betrouwbaarheid); //een pseudo random boolean
  var delayMs = 1000 + (Math.random() * 100); //pseudo random delay van minimaal 1sec

  //na de delay wordt een resolve of reject uitgevoerd
  //er wordt gebruikt gemaakt van conditional (ternary) operator zie https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator
  setTimeout(() => {
    resultaat ? resolve(naam + ' heeft zich aan de belofte gehouden') : reject(naam + ' heeft de belofte verbroken');
  }, delayMs);
});

//start van de voorbeelden
log('💕 Beloofd is beloofd: Vanilla JS playground 💔');

/** ********************************************
  * Voorbeeld 1, een enkele promise afhandelen met een then-catch chain
  */
var beloofdIsBeloofd = new Promise((resolve, reject) => {
  belofte(resolve, reject, 'beloofdIsBeloofd', 0.5);
});

//verwerken van de promise, is het beloofd is beloofd?
beloofdIsBeloofd.then((resultaatResolve) => {
  //de belofte is WEL gehouden the 'then' function
  log(resultaatResolve);
}).catch((resultaatReject) => {
  //de belofte is NIET gehouden the 'catch' function
  log(resultaatReject);
});

//deze wordt eerst uitgevoerd
log('Duurt lang..');


/** ********************************************
  * Voorbeeld 2, twee promises, verwerken met een promise (met ALL en race)
  * info: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
  */

//defineer twee promises, in een functie zodat deze kan worden hergebuikt in voorbeeld
var beloftePartner1 = () => {
  return new Promise((resolve, reject) => {
    belofte(resolve, reject, 'P1', 0.2);
  });
};

var beloftePartner2 = () => {
  return new Promise((resolve, reject) => {
    belofte(resolve, reject, 'P2', 0.5);
  });
};

//onderstaande promise geeft een resolve als beide promises geresolved worden
Promise.all([beloftePartner1(), beloftePartner2()]).then((result) => {
  log('P1 & P2: 💕 Sterke relatie (' + result + ')');
}).catch((result) => {
  log('P1 & P2: 💔 Oops, terug naar Tinder (' + result + ')');
});

//onderstaande promise geeft een resolve als één van beide geresolved wordt
Promise.race([beloftePartner1(), beloftePartner2()]).then((result) => {
  log('P1 & P2 (poging 2): 💕 Wie was als eerste met de belofte? (' + result + ')');
}).catch((result) => {
  log('P1 & P2 (poging 2): 💔 Oops, geen van beide geeft een promise (' + result + ')');
});
