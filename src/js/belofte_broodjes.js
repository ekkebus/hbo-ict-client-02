/**
  * [1] Maak een function bestelBroodje. 
  * 
  * Geef de function één parameter: naam. 
  * De function retourneert een Promise. (de "Bestelling")
  * De resolved waarde van de Promise bevat een object met twee properties: naam en prijs.
  *
  * [4a] als de naam van het broodje niet ‘broodje kroket’ of ‘broodje kaas’ is dan wordt er een error gegooit. Gebruik reject.
  **/
let bestelBroodje = function (naam) {
    log(`[1] bestelBroodje({${naam}})`);
    return new Promise(function (resolve, reject) {
        if (naam == 'broodje kroket' || naam == 'broodje kaas') {
            resolve({ naam: naam, prijs: 0 });
        } else {
            reject(`Een ${naam} hebben we niet in het assortiment. Sorry.`);
        }
    });
}

/**
  * [2a] Maak een function broodjeKlaarmaken. 
  * 
  * Geef de function één parameter: bestelling. (de resolve vanuit bestelbroodje)
  * De function retourneert een Promise. 
  * De function maakt het broodje klaar (resolved), maar dit duurt twee seconden
  * 
  **/
let broodjeKlaarmaken = function (bestelling) {
    log(`[2] broodjeKlaarmaken({${bestelling.naam},${bestelling.prijs}})`);
    return new Promise(function (resolve, reject) {
        setTimeout(resolve(bestelling), 2000);
    });
}

/**
  * [2b] Roep de methode bestelBroodje aan en chain vervolgens met de function broodjeKlaarmaken. 
  * [3b] Voeg een aanroep naar bestellingAfrekenen toe aan de eerdere chain. 
  * [4b] Zorg dat de error wordt opgevangen door een catch function toe te voegen aan het einde van de chain. 
  **/
bestelBroodje('broodje kroket')
    .then(bestelling => broodjeKlaarmaken(bestelling))
    .then(bestelling => bestellingAfrekenen(bestelling))
    .then(bestelling => log(`Een enkele bestelling: ${bestelling.naam} voor ${bestelling.prijs}`))
    .catch(error => log(error));

/**
  * [3a] Maak een function bestellingAfrekenen.
  * 
  * Geef de function één parameter: bestelling. (de resolve vanuit broodjeKlaarmaken)
  * De function retourneert een Promise. De function berekent de prijs van het broodje. 
  * Een broodje kroket kost € 2,50 en een broodje kaas kost € 1,80.
  **/
let bestellingAfrekenen = function (bestelling) {
    log(`[3] bestellingAfrekenen({${bestelling.naam},${bestelling.prijs}})`);
    return new Promise(function (resolve, reject) {

        if (bestelling.naam === 'broodje kroket') {
            bestelling.prijs = 2.50;
        } else if (bestelling.naam === 'broodje kaas') {
            bestelling.prijs = 1.80;
        } else {
            reject(`Een ${naam} hebben we niet in het assortiment. Sorry.`);
        }

        resolve(bestelling);
    });
}

/**
  * [5] Bestel nu twee broodjes tegelijk … en het mislukt. 
  * Zorg ervoor dat de errors worden opgevangen in de .catch van de Promise.all.
  * Als de bestellingen slagen moeten de bedragen van de broodjes bij elkaar worden opgeteld.
  * Verder moeten de broodjes toegevoegd worden aan een array met broodjes.
  */

let bestellingA = bestelBroodje('broodje kroket');
let bestellingB = bestelBroodje('broodje kip');

Promise.all([bestellingA, bestellingB]).then(function (bestellingen) {
    let pAll = [];
    for (let bestelling of bestellingen) {
        pAll.push(broodjeKlaarmaken(bestelling));
    }

    return Promise.all(pAll);
}).then(function (bestellingen) {
    let pAll = [];
    for (let bestelling of bestellingen) {
        pAll.push(bestellingAfrekenen(bestelling));
    }

    return Promise.all(pAll);
}).then(function (bestellingen) {
    let totaal = 0;
    for (let bestelling of bestellingen) {
        totaal += bestelling.prijs;
    }
    log(`💶 Het totaalbedrag is ${totaal}`);

    //BONUS: of bereken het met met REDUCE, zie https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
    let totalAccumulator = bestellingen.reduce((accumulator, current) => accumulator + current.prijs, 0);
    log(`💶 Het totaalbedrag is nog steeds ${totalAccumulator}`);
}).catch(function (error) {
    log(`🙀 Oops: ${error}`);
});