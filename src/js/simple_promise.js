/***
 * Belofte om twee kamers op te ruimen (slaapkamer en woonkamer)
 */
let slaapkamerOpruimen = new Promise(function (resolve, reject) {
    let isOpgeruimd = true;

    if (isOpgeruimd) {
        resolve('Schoon')
    } else {
        reject('Bweee, de slaapkamer is niet schoon')
    }
}).catch(function (error) {
    throw Error(error)
});

slaapkamerOpruimen.then(function(schoonOfNiet){
    console.log(`De slaapkamer is ${schoonOfNiet}`);
}).catch(function(schoonOfNiet) {
    console.log(`De slaapkamer is ${schoonOfNiet}`);
});

//oei er is nog een kamer
let woonkamerOpruimen = new Promise(function (resolve, reject) {
    let isOpgeruimd = true;

    if (isOpgeruimd) {
        resolve('Schoon')
    } else {
        reject('Bweee, de woonkamer is niet schoon')
    }
}).catch(function (error) {
    throw Error(error)
});

//als beide promisses vervuld zijn
Promise.all([slaapkamerOpruimen, woonkamerOpruimen])
    .then(result => {
        log("YES! " + result)
    }).catch(e => log(`Catched in .all: ${e}`));