SPA.Popup = (function($){
    let _root;
    let _popup;

    function _initModule(root){
        _root = root;   
    }

    function _show(bericht){
        if (_popup){
            _remove();
        }

        _popup = $("<div>"+bericht+"</div>");
        let knop = $("<input type='button' value='X'/>");
        $(knop).click(_remove);
        
        $(_popup).append(knop);
        $(_root).append(_popup);
    }

    function _remove(){
        $(_popup).remove();
        _popup = undefined;
    }

    //publieke interface van SPA.Popup
    return {
            init:_initModule,
            show:_show
    }
})($);