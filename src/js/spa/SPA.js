const SPA = (function($){

    function _initModule(root){
        SPA.Popup.init(root);
        let button = $("<input type='button' value='dont click me'>");
        $(button).click(_showWeather);
        $(root).append(button);
    }

    function _showWeather(){
        var veenendaal = SPA.Weer.getWeather("Veenendaal");
        var zwolle = SPA.Weer.getWeather("Zwolle");

        Promise.all([veenendaal,zwolle]).then(function(values){
            var bericht='🌡️';
            $(values).each(function(index,value){
                bericht+=value.main.temp+" ";
            })
            SPA.Popup.show(bericht);
        }).
        catch(function(data){
            SPA.Popup.show(data);
        })
    }

    //publieke interface van SPA
    return {
        init:_initModule
    }
})(jQuery);

