SPA.Weer = (function ($) {
    //maak je eigen API key aan op openweathermap.org
    var _configMap = {
        apikey: 'XYZ'
    };

    function _getWeather(locatie) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/weather?q=' + locatie + '&APPID=' + _configMap.apikey,
                success: function (data) {
                    resolve(data);
                },
                error: function (data) {
                    reject("Kan de data niet ophalen");
                }

            })
        });
    }

    //publieke interface van SPA.Weer
    return {
        getWeather: _getWeather
    }

})(jQuery);