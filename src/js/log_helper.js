/**
 * Helper 'log' functie die naar body print
 *
 * @att data, string
 *
 * Voorbeeld gebruik: log('het bericht wat je wilt loggen')
 */
var log = (function (data) {
    console.log(data);
    var logItem = document.createElement('p');
    logItem.innerHTML = data;
    document.getElementById("console").appendChild(logItem);
});