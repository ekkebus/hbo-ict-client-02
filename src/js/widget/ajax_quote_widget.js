ajax_quote_widget = (function () {
    let _init, _domQuery, _makeApiCall, _getQuote, _updateDOM

    //This aSync function makes the API call and returns a promise with JSON from the server
    _makeApiCall = function () {
        //for more 'open' API's see: https://github.com/toddmotto/public-apis
        //http://api.quotable.io/random
        return fetch('https://programming-quotes-api.herokuapp.com/quotes/random')
            .then(data => {
                return data.json()
            })
    }

    //this aSync function transforms the ApiCall into a Single String and returns it as a Promise
    _getQuote = function () {
        //hack: to make code runnable for Jasmine CLI
        let makeApiCallAsyncFunction = (typeof this.makeApiCall === 'undefined') ? _makeApiCall : this.makeApiCall;

        return new Promise((resolve, reject) =>
            makeApiCallAsyncFunction()
                .then(json => {
                    _updateDOM(json)
                    return `${json.en} by ${json.author}`
                }).then(quote => {
                    resolve(quote)
                })
                .catch(error => {
                    reject(error)
                })
        )//end of Promise object
    }

    //function which updates the DOM, appends the forcast data to div
    _updateDOM = function (json) {
        //hack: for jasmine since the 'document' object does not exist in CLI by default:)
        if (typeof document !== 'undefined')
            document.querySelector(_domQuery).innerHTML += '<pre><code>' + JSON.stringify(json) + '</code></pre>'
    };

    _init = function (domQuery) {
        _domQuery = domQuery   //set the 'private' variable, used in _updateDOM function
        setInterval(_getQuote, 3000)
    }

    //Public interface of this module
    return {
        init: _init,
        getQuote: _getQuote,
        makeApiCall: _makeApiCall
    }
})();
