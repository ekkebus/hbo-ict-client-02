# hbo-ict-client-02

Run instructions
```
$ git clone https://gitlab.com/ekkebus/hbo-ict-client-02.git
npm init
```
Run test instructions
```
$ jasmine
```
OR
```
$ gulp test
```

Installed NPM packages
```
npm install --save-dev gulp jasmine gulp-jasmine
```

