//require target under testing
require('../src/js/widget/ajax_quote_widget.js')

/** 
 * -------------
 * Check spa model by mocking the spa.data layer
 * -------------
*/
describe("Ajax Quote Widget (mocking AJAX call)", function () {

    let testJson = {
        "_id": "777",
        "en": "Do not worry about tomorrow, for tomorrow will worry about itself.",
        "author": "Jesus Christ",
        "id": "777"
    }

    describe("Test ajax_quote_widget.getQuote() resolved", function () {
        beforeEach(function () {
            //add a spies and mock its behaviour
            spyOn(ajax_quote_widget, 'makeApiCall').and.callFake(function () {
                //this will always resolve and insert/return the test data
                return Promise.resolve(testJson);
            });
        });

        it("Test if the test data is being retruned (and if the AJAX is mocked)", async function () {
            let quote = await ajax_quote_widget.getQuote()
            let expectedQuote = `${testJson.en} by ${testJson.author}`;
            expect(quote).toBe(expectedQuote);
        });
    })

    describe("Test ajax_quote_widget.getQuote() reject", function () {
        beforeEach(function () {
            //add a spies and mock its behaviour
            spyOn(ajax_quote_widget, 'makeApiCall').and.callFake(function () {
                //this will always resolve and insert/return the test data
                return Promise.reject(new Error('Ooops just testing :)'));
            });
        });

        //Please note the callbackfunction 'done' attribute
        it("Test if the expected error is returned", function (done) {
            ajax_quote_widget.getQuote()
                .then(() => done(new Error('This should not happen')))
                .catch((error) => {
                    expect(error.message).toBe('Ooops just testing :)')
                    done()
                })
        });
    })
});