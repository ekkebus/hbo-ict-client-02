const gulp = require('gulp');
const jasmine = require('gulp-jasmine');
 
const runJasmineTests = () => {
    return gulp.src('./spec/**/*[sS]pec.js')
        .pipe(jasmine())//execute test here :)
}

//export the GULP methods
exports.test = runJasmineTests
